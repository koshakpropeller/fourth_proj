<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * Интернет-программирование. Задача 8.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  $errors = array();
  $errors['name_empty'] = !empty($_COOKIE['name_error_empty']);
  $errors['name_mistype'] = !empty($_COOKIE['name_error_mistype']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['birth'] = !empty($_COOKIE['birth_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['counter'] = !empty($_COOKIE['counter_error']);
  $errors['options'] = !empty($_COOKIE['options_error']);
  $errors["info"] = !empty($_COOKIE['info_error']);
  $errors["checked"] = !empty($_COOKIE['checked_error']);
  $errors['name'] = $errors['name_empty'] || $errors['name_mistype'];
    
  if($errors['name_empty']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('name_error_empty',"", 100000);
       // Выводим сообщение.
       $messages[] = '<div class="error">Заполните имя.</div>';
  }
   if($errors['name_mistype']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('name_error_mistype',"", 100000);
       // Выводим сообщение.
       $messages[] = "<div class='error'>Имя должно состоять только из русский или английских букв и пробелов</div>";
  }
  if($errors['email']) {
      setcookie('email_error', "", 100000);
      $messages[] = '<div class="error">Заполните поле email.</div>';
  }
  if($errors['birth']) {
      setcookie('birth_error',"",100000);
      $messages[] = "<div class='error'>Заполните поле дата рождения</div>";
  }
  if($errors['gender']) {
      setcookie('gender_error', "",100000 );
      $messages[] = "<div class='error'>Выберете пол</div>";
  }
  if($errors['counter']){
      setcookie('counter_error', "", 100000);
      $messages[] = "<div class='error'>Выберете количество конечностей</div>";
  }
  if($errors['options']) {
      setcookie('options_error', "", 100000);
      $messages[] = "<div class='error'>А где ваши способности?</div>";
  }
  if($errors['info']) {
      setcookie('info_error',"",100000);
      $messages[] = "<div class='error'>Кто вы?Поподробнее</div>";
  }
  if($errors["checked"]){
      setcookie('checked_error',"",100000);
      $messages[] = "<div class='error'>Согласитесь с нами</div>";
  }
    
  // Складываем предыдущие значения полей в массив, если есть.
  $values=array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['birth'] = empty($_COOKIE['birth_value']) ? '' : $_COOKIE['birth_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['counter'] = empty($_COOKIE['counter_value']) ? '' : $_COOKIE['counter_value'];
  $options_value = array();
  $options_value['nodeath'] = FALSE;
  $options_value['through'] = FALSE;
  $options_value['levitation'] = FALSE;
  $values['options'] = empty($_COOKIE['options_value']) ? $options_value : json_decode($_COOKIE['options_value'], true);
  $values['info'] = empty($_COOKIE['info_value']) ? '' : $_COOKIE['info_value'];
  $values['checked'] = empty($_COOKIE['checked_value']) ? '' : $_COOKIE['checked_value'];
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
// Проверяем ошибки.
$errors = FALSE;
$flag = array();
$flag["name"] = TRUE;
$flag["email"] = TRUE;
$flag["birth"] = TRUE;
$flag["gender"] = TRUE;
$flag["counter"]= TRUE;
$flag["options"]= TRUE;
$flag["info"] = TRUE;
$flag["checked"] = TRUE;
if (empty($_POST['name']) || strlen($_POST['name']) > 128) {
  setcookie('name_error_empty', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
  $flag["name"] = FALSE;
}
if(preg_match('/[^а-яА-Яa-zA-Z\s]+/musi',$_POST['name'])) {
    setcookie('name_error_mistype', '1', time() + 24 * 60 * 60);
    $errors = TRUE;

    $flag["name"] = FALSE;
}
if (empty(trim($_POST['email']))) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
    $flag["email"]= FALSE;
}
if (empty(trim($_POST['birth']))) {
    setcookie('birth_error', '1',  time() + 24 * 60 * 60);
    $errors = TRUE;
    $flag["birth"] = FALSE;
}
if (!isset($_POST['gender'])) {
    setcookie('gender_error', '1',  time() + 24 * 60 * 60);
    $flag["gender"] = FALSE;
    $errors = TRUE;
}
if (!isset($_POST['counter'])) {
   setcookie('counter_error', '1',  time() + 24 * 60 * 60);
    $errors = TRUE;
    $flag["counter"] = FALSE;
}
if (!isset($_POST["options"])) {
   setcookie('options_error', '1',  time() + 24 * 60 * 60);
    $errors = TRUE;
    $flag["options"] = FALSE;
}
if (empty(trim($_POST["info"]))) {
    setcookie('info_error', '1',  time() + 24 * 60 * 60);
    $errors = TRUE;
    $flag["info"] = FALSE;
}
if(!isset($_POST["checked"])) {
    setcookie('checked_error', '1',  time() + 24 * 60 * 60);
    $errors = TRUE;
    $flag["checked"] = FALSE;
}
// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
if($flag["name"]) {
    setcookie('name_value', $_POST['name'], time() + 24 * 60 * 60);
}
if($flag["email"]) {
    setcookie('email_value', $_POST['email'], time() + 24 * 60 * 60);
}
if($flag["birth"]) {
    setcookie('birth_value', $_POST['birth'], time() + 24 * 60 * 60);
}
if($flag["gender"]){
    setcookie('gender_value', $_POST['gender'],  time() + 24 * 60 * 60);
}
if($flag["counter"]){
    setcookie("counter_value", $_POST["counter"], time() + 24 * 60 * 60);
}
if($flag["options"]){
    $options_cookie = array();
    $options_cookie["nodeath"] = in_array("nodeath",$_POST["options"]);
    $options_cookie["through"] = in_array("through",$_POST["options"]);
    $options_cookie["levitation"] = in_array("levitation", $_POST["options"]);
    setcookie("options_value", json_encode($options_cookie), time() + 24 * 60 * 60);
}
if($flag["info"]) {
    setcookie("info_value", $_POST["info"], time() + 24 * 60 * 60);
}
if($flag["checked"]) {
    setcookie("checked_value", TRUE, time() + 24 * 60 * 60);
}
if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  header('Location: index.php');
  exit();
}
else {
    setcookie('name_error_empty', '', 100000);
    setcookie('name_error_mistype', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('birth_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('counter_error', '', 100000);
    setcookie('options_error', '', 100000);
    setcookie('info_error', '', 100000);
    setcookie('checked_error', '', 100000);
}
// Сохранение в базу данных.

$user = 'u20386';
$pass = '2551027';
$db = new PDO('mysql:host=localhost;dbname=u20386', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.



try {
  $stmt = $db->prepare("INSERT INTO app (name, email, year, gender, lungs, nodeath, through, levitation, biography) VALUES (:name, :email, :year, :gender, :lungs, :nodeath, :through, :levitation, :biography)");
  $name = $_POST['name'];
  $stmt->bindParam(':name', $name, PDO::PARAM_STR);
  
  $email = $_POST['email'];
  $stmt->bindParam(':email', $email,PDO::PARAM_STR);
 
  $year = $_POST['birth'];
  $stmt->bindParam(':year', $year, PDO::PARAM_INT); 
  
  $gender = $_POST['gender'];
  $stmt->bindParam(':gender', $gender, PDO::PARAM_INT); 
  
  $counter = $_POST['counter'];
  $stmt->bindParam(':lungs', $counter, PDO::PARAM_INT);
  
  $nodeath = in_array("nodeath",$_POST["options"]);

  $stmt->bindParam(':nodeath', $nodeath, PDO::PARAM_BOOL);
  
  $through = in_array("through",$_POST["options"]);

  $stmt->bindParam(':through', $through,PDO::PARAM_BOOL);
  
  $levitation = in_array("levitation", $_POST["options"]);
  
  $stmt->bindParam(':levitation', $levitation,PDO::PARAM_BOOL);
  
  $biography = $_POST["info"];
  print($biography);
  $stmt->bindParam(':biography', $biography, PDO::PARAM_STR);
  
 
 
  $stmt -> execute();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
setcookie('save', '1');
header('Location: index.php');
}
