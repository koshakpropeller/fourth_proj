<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css"/>
        <title>Third Task</title>
        <style>
       /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
      .error {
        border: 2px solid red;
     }
    </style>

    </head>
    <body>
        <?php
          if(!empty($messages)) {
              print('<div id="messages">');
              foreach($messages as $message){
                  print($message);
              }
              print('</div>');
          }
        ?>
        <form id="myForm" method="POST" action="">
			<label>
				 Имя: <br>
				 <input type="text" name="name" 
                        <?php if($errors['name']) print 'class="error" ';
                        ?>
                        value="<?php print $values['name'] ; ?>"
                        />
	                   
			</label>
			<br>
			<label>
			  Email: <br>
			  <input type="email" name="email" 
                     <?php if($errors['email']) {
                          print 'class="error"';
                       }
                     ?>
                     value="<?php print $values['email'] ; ?>"
                     />
	  
			</label>
	  
			<br>
		   <label>
		  Дата рождения: <br>
		  <select name="birth" 
                  <?php if($errors['birth']) {
                      print 'class="error"';
                     }
                  ?>
                  
                  >


                     <?php
                     for($i = 2000; $i < 2004; $i++) {
                         if ((int)$values['birth'] == $i) {
                             echo "<option value='" . $i . "' selected >". $i . "</option>";
                         } else {
                             echo "<option value='" . $i . "'>". $i . "</option>";
                         }
                     }
                     ?>

		  </select>
	  
		  </label>
		 <br>
		 Пол:<br>
        <div 
             <?php if($errors['gender']) {
                      print 'class="error" ';
                     }
                  ?>>
		 <label>

		  <?php
		     if($values['gender'] == 0) {
		         echo '<input type="radio" value="0" name="gender" checked>Женский';
             }
		     else {
                 echo '<input type="radio" name="gender" value="0" >Женский';
             }
		  ?>
		 </label>
		 <br>
		<label>
            <?php
            if($values['gender'] == 1) {
                echo '<input type="radio" value="1" name="gender" checked>Мужской';
            }
            else {
                echo '<input type="radio" name="gender" value="1" >Мужской';
            }
            ?>
		</label>
       </div>
		
		Количество конечностей:
		<br>
        <div <?php if($errors['counter']) {
                      print 'class="error" ';
                     }
                  ?>
             >
		<label>
            <?php
              if($values['counter'] == 0) {
                  echo '<input type="radio" checked name="counter" value="0">0';
              }
              else {
                  echo '<input type="radio" name="counter" value="0">0';
              }
            ?>
		</label>
	   <br>
		<label>
            <?php
            if($values['counter'] == 'gt0') {
                echo '<input type="radio" checked name="counter" value="gt0">>0';
            }
            else {
                echo '<input type="radio" name="counter" value="gt0">>0';
            }
            ?>

		</label>
		
        </div>
		<label>
		  Сверхспособности:
		  <br>

		  <select name="options[]" multiple
                  <?php
                    if($errors['options']) {
                        print 'class="error" ';
                    }
                  ?>
                  >
              <?php

              if($values['options']['nodeath']){
                  echo '<option value="nodeath" selected>бессмертие</option>';
              }
              else {
                  echo '<option value="nodeath">бессмертие</option>';
              }
              ?>
              <?php
                if($values['options']['through']) {
                    echo '<option value="through" selected>прохождение сквозь стены</option>';
                }
                else {
                    echo '<option value="through">прохождение сквозь стены</option>';
                }
              ?>
              <?php
              if($values['options']['levitation']) {
                  echo '<option value="levitation" checked>левитация</option>';
              }
              else {
                  echo '<option value="levitation" >левитация</option>';
              }
              ?>

		  </select>
		</label>
		<br>
		<label>
		  Биография:
		  <br>
		  <textarea name="info"
                    <?php
                      if($errors["info"]) {
                          print 'class="error" ';
                      }
                    ?>
                    >
              <?php print($values["info"]); ?>
		  </textarea>
		</label>
	   <br>
		<label>
		  
		  <input type="checkbox"
                 <?php 
                   if($errors["checked"]) {
                        print 'class="error" ';
                   }
                 ?>
                 <?php
                   if($values['checked']) {
                       print 'checked';
                   }
                 ?>
                 name="checked">
		  С контрактом ознакомлен
		</label>
	  
	   <br>
		  <input type="submit" value="Отправить">
		</form>
    </body>
</html>